import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Zamowienie {

    List<Pozycja> zamowienie = new ArrayList<Pozycja>();


    @Getter
    @Setter
    int ileDodanych;

    @Getter
    int maxRozmiar;

    public void setMaxRozmiar(int maxRozmiar) {
        if (maxRozmiar <= 0) {

            throw new IllegalArgumentException("Podaj wartość większą od 0");
        }

        this.maxRozmiar = maxRozmiar;
    }

    public Zamowienie() {
        this.maxRozmiar = 11;
    }

    public Zamowienie(int maxRozmiar) {
        this.maxRozmiar = maxRozmiar;
    }

    public void dodajPozycje(Pozycja p) {

        zamowienie.add(p);
    }

    public double obliczWartoscZamowienia() {

        double suma = 0;

        for (Pozycja p : zamowienie) {

            suma += p.obliczWartosc();
        }

        return suma;
    }



    public String toString() {

        String result = "Zamówienie:" + "\n";


        for (Pozycja p : zamowienie) {

           result += p.toString() + "\n";
        }

        result +="\n" + "Razem: " + obliczWartoscZamowienia();

        return result;

    }

}
