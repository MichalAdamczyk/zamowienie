import lombok.Getter;
import lombok.Setter;

public class Pozycja {


    @Getter
    private String nazwaTowaru;

    public void setNazwaTowaru(String nazwaTowaru) {

        if(nazwaTowaru == null){

            throw new IllegalArgumentException("złe dane");
        }

        this.nazwaTowaru = nazwaTowaru;
    }


    @Getter
    private int ileSztuk;

    public void setIleSztuk(int ileSztuk) {


        if(ileSztuk <= 0){

            throw new IllegalArgumentException("ilość musi być większa od 0");

        }
        this.ileSztuk = ileSztuk;
    }

    @Getter
    private double cena;


    public void setCena(double cena) {

        if(cena <= 0){

            throw new IllegalArgumentException("złe dane");
        }

        this.cena = cena;
    }


    public Pozycja(String nazwaTowaru, int ileSztuk, double cena) {
        this.nazwaTowaru = nazwaTowaru;
        this.ileSztuk = ileSztuk;
        this.cena = cena;
    }

    public double obliczWartosc(){

        return ileSztuk*cena;
    }

    public String toString(){

        return nazwaTowaru + "\t" + String.format("%10s", cena) + " zł" + " "
               + String.format("%4s", ileSztuk) + " szt." + "\t" + String.format("%10s", obliczWartosc() + " zł");
    }


}
