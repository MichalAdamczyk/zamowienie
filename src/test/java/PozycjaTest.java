import org.junit.Test;

import static org.junit.Assert.*;

public class PozycjaTest {




    @Test
    public void testObliczWartosc() {

        //Given
        Pozycja cukier = new Pozycja("masło", 20, 3);

        //When

        double result = cukier.obliczWartosc();

        //Then

        assertEquals(Double.valueOf(60), Double.valueOf(result));
    }


    @Test
    public void String() {

        //Given
        Pozycja marchew = new Pozycja("marchew", 50, 0.30);

        //When

        String message = marchew.toString();

        //Then

        assertTrue(message.contains("marchew"));

    }

    @Test
    public void getNazwaTowaru() {
        //Given

        Pozycja mleko = new Pozycja("mleko", 10, 6);

        //When

        String nazwa = mleko.getNazwaTowaru();

        //Then

        assertEquals("mleko", nazwa);
    }

    @Test
    public void getIleSztuk() {
        //Given
        Pozycja kokos = new Pozycja("kokos", 1, 10);
        //When

        int ilosc = kokos.getIleSztuk();

        //Then
        assertEquals(1, ilosc);
    }

    @Test
    public void getCena() {

        //Given

        Pozycja cukier = new Pozycja("cukier", 1, 5);

        //When

        double cena = cukier.getCena();

        //Then

        assertEquals(Double.valueOf(5), Double.valueOf(cena));

    }

    @Test (expected = IllegalArgumentException.class)
    public void setNazwaTowaru() {
        //Given
        Pozycja piwo = new Pozycja("piwo", 5, 2);
        //When
        piwo.setNazwaTowaru(null);

        //Then
        assertEquals("miód", piwo.getNazwaTowaru());
    }


    @Test
    public void testSetNazwaTowaru() {
        //Given
        Pozycja piwo = new Pozycja("piwo", 5, 2);
        //When
        piwo.setNazwaTowaru("miód");

        //Then
        assertEquals("miód", piwo.getNazwaTowaru());
    }


    @Test (expected = IllegalArgumentException.class)
    public void setIleSztuk() {
        //Given

        Pozycja bułka = new Pozycja("bułka", 6, 1.50);
        //When
        bułka.setIleSztuk(-3);

        //Then
        assertEquals(-3, bułka.getIleSztuk());

    }



    @Test
    public void testSetIleSztuk() {
        //Given

        Pozycja bułka = new Pozycja("bułka", 6, 1.50);
        //When
        bułka.setIleSztuk(6);

        //Then
        assertEquals(6, bułka.getIleSztuk());

    }


    @Test (expected = IllegalArgumentException.class)
    public void setCena() {
        //Given
        Pozycja wino = new Pozycja("wino", 1, 15);

        //When
        wino.setCena(0);

        //Then

        assertEquals(Double.valueOf(0), Double.valueOf(wino.getCena()));

    }

    @Test
    public void testSetCena() {
        //Given
        Pozycja wino = new Pozycja("wino", 1, 15);

        //When
        wino.setCena(15);

        //Then

        assertEquals(Double.valueOf(15), Double.valueOf(wino.getCena()));

    }


}