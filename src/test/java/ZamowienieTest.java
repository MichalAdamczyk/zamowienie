import org.junit.Test;

import static org.junit.Assert.*;

public class ZamowienieTest {

    @Test
    public void dodajPozycje() {
        //Given


        //When

        //Then

    }

    @Test
    public void obliczWartoscZamowienia() {
        //Given

        //When

        //Then

    }

    @Test
    public void getIleDodanych() {
        //Given


        //When

        //Then

    }

    @Test
    public void getMaxRozmiar() {
        //Given
        Zamowienie zamowienie1 = new Zamowienie();

        //When
        int result = zamowienie1.getMaxRozmiar();

        //Then
        assertEquals(10, result);

    }


    @Test
    public void TestGetMaxRozmiar() {
        //Given
        Zamowienie zamowienie2 = new Zamowienie(45);

        //When
        int result = zamowienie2.getMaxRozmiar();

        //Then
        assertEquals(45, result);
    }


    @Test (expected = IllegalArgumentException.class)
    public void setMaxRozmiar() {
        //Given
        Zamowienie zamowienie3 = new Zamowienie();

        //When
        zamowienie3.setMaxRozmiar(-456);

        //Then
        assertEquals(-456, zamowienie3.getMaxRozmiar());
    }


    @Test
    public void TestSetMaxRozmiar() {
        //Given
        Zamowienie zamowienie3 = new Zamowienie();

        //When
        zamowienie3.setMaxRozmiar(23);

        //Then
        assertEquals(23, zamowienie3.getMaxRozmiar());
    }




    @Test
    public void setIleDodanych() {
        //Given

        //When

        //Then

    }
}